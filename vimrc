" Robin's stuff
set background=dark
set number
set encoding=utf-8
set rtp+=~/.vim/bundle/vim-vividchalk
colorscheme vividchalk
"set rtp+=~/.vim/bundle/vim-colors-solarized
"colorscheme solarized


" Vundle config
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'tpope/vim-vividchalk'
Plugin 'chrisbra/vim-show-whitespace'
Plugin 'chrisbra/Recover.vim'
Plugin 'powerline/powerline' 
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
"Plugin 'klen/python-mode'
Plugin 'davidhalter/jedi-vim'
Plugin 'editorconfig/editorconfig-vim'
call vundle#end()
filetype plugin indent on


" Powerline config
set laststatus=2
set t_Co=256
set term=xterm-256color
set termencoding=utf-8
set guifont=Ubuntu\ Mono\ derivative\ Powerline:10
let g:Powerline_symbols = 'fancy'

" Nerdtree
map <F2> :NERDTreeToggle<CR>

"" Pymode
"
"" Auto-completion
"let g:pymode_rope = 1
"
"" Documentation
"let g:pymode_doc = 1 
"let g:pymode_doc_key = 'K'
"
"" Linting
"let g:pymode_lint = 1
"let g:pymode_lint_checker = 'pyflakes,pep8'
"let g:pymode_lint_write = 1
"
"" Don't autofold code
"let g:pymode_folding = 0




" Move between split with only one keypress
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

" enable per project .vimrc files
" as per http://andrew.stwrt.ca/posts/project-specific-vimrc/
"set exrc
"set secure
